`include "gm_header.vh"

module goldenmodel(
    input rst,
    input clk,
    output [`A_SIZE-1:0] pc,
    input [15:0] instruction,
    output read,
    output write,
    output [`A_SIZE-1:0]	address,
    input  [`D_SIZE-1:0]	data_in,
    output [`D_SIZE-1:0]	data_out
);

reg [`D_SIZE-1:0] RegSet[0:7];
reg [`D_SIZE-1:0] data [0:`A_SIZE-1];

always@(posedge clk or posedge rst) begin
if(!rst)
	begin
		pc = 0;
   		for(i = 0; i< 8; i = i + 1)
		begin
			RegSet[i] = i;
		end
		for(i=0;i<1023;i=i+1)
		begin
			data[i]=i*2;
		end
	end
else
	begin
	case(instruction[15:9])
		`ADD	: begin RegSet[instruction[`OP0]]=RegSet[instruction[`OP1]]+RegSet[instruction[`OP2]];pc=pc+1;end
		`ADDF	: begin RegSet[instruction[`OP0]]=RegSet[instruction[`OP1]]+RegSet[instruction[`OP2]];pc=pc+1; end
		`SUB	: begin RegSet[instruction[`OP0]]=RegSet[instruction[`OP1]]-RegSet[instruction[`OP2]];pc=pc+1; end
		`SUBF	: begin RegSet[instruction[`OP0]]=RegSet[instruction[`OP1]]-RegSet[instruction[`OP2]];pc=pc+1; end
		`AND  : begin RegSet[instruction[`OP0]] = RegSet[instruction[`OP1]] & RegSet[instruction[`OP2]]; pc=pc+1;end 
	   `OR   : begin RegSet[instruction[`OP0]] = RegSet[instruction[`OP1]] | RegSet[instruction[`OP2]]; pc=pc+1;end  
	   `XOR  : begin RegSet[instruction[`OP0]] = RegSet[instruction[`OP1]] ^ RegSet[instruction[`OP2]]; pc=pc+1;end 
	   `NAND : begin RegSet[instruction[`OP0]] = !(RegSet[instruction[`OP1]] & RegSet[instruction[`OP2]]); pc=pc+1;end 
	   `NOR  : begin RegSet[instruction[`OP0]] = ~(RegSet[instruction[`OP1]] | RegSet[instruction[`OP2]]); pc=pc+1;end 
	   `NXOR : begin RegSet[instruction[`OP0]] = ~(RegSet[instruction[`OP1]] ^ RegSet[instruction[`OP2]]); pc=pc+11;end 
	   `SHIFTR: begin RegSet[instruction[`OP0]] = RegSet[instruction[`OP1]] >> (instruction[`OP2] +1); pc=pc+1;end 
	   `SHIFTRA: begin RegSet[instruction[`OP0]] = RegSet[instruction[`OP1]] >>> (instruction[`OP2] +1); pc=pc+1;end 
	   `SHIFTL : begin RegSet[instruction[`OP0]] = RegSet[instruction[`OP1]] >> (instruction[`OP2] +1); pc=pc+1;end 
		`LOAD  : begin RegSet[instruction[`LS_OP0]] = data[RegSet[instruction[`LS_OP1]]]; pc=pc+1; end 
		`LOADC : begin RegSet[instruction[`LS_OP0]] = {RegSet[instruction[`LS_OP0]][15:8],instruction[7:0]}; pc=pc+1; end 
		`STORE : begin data[RegSet[instruction[`LS_OP0]]] = RegSet[instruction[`LS_OP1]]; pc=pc+1; end 
		`STOREC: begin data[RegSet[instruction[`LS_OP0]]] = {8'b0,instruction[7:0]}; pc=pc+1;end 
		`JMP  : begin pc = RegSet[instruction[2:0]]; end
		`JMPR : begin pc = pc + instruction[5:0]; end
		`JMPcond: begin
				case(instruction[`cond])
					`N: begin if(RegSet[instruction[8:6]]<0)  begin pc=RegSet[instruction[2:0]]; end end
					`NN:begin if(RegSet[instruction[8:6]]>=0) begin pc=RegSet[instruction[2:0]]; end end
					`Z: begin if(RegSet[instruction[8:6]]==0) begin pc=RegSet[instruction[2:0]]; end end
					`NZ:begin if(RegSet[instruction[8:6]]!=0) begin pc=RegSet[instruction[2:0]]; end end
				endcase
			  end
		`JMPRcond:begin 
				case(instruction[`cond])
					`N: begin if(RegSet[instruction[8:6]]<0)  begin pc = pc + instruction[5:0]; end end
					`NN:begin if(RegSet[instruction[8:6]]>=0) begin pc = pc + instruction[5:0]; end end
					`Z: begin if(RegSet[instruction[8:6]]==0) begin pc = pc + instruction[5:0]; end end
					`NZ:begin if(RegSet[instruction[8:6]]!=0) begin pc = pc + instruction[5:0]; end end
				endcase
			end
endcase
end

end

endmodule